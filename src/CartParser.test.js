import CartParser from './CartParser';
import * as uuid from 'uuid';
import { readFileSync } from 'fs';

let parser;

beforeEach(() => {
	parser = new CartParser();	
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.

	// #1
	it('should return object with id generated by uuid module, and parsed name, price, quantity', () => {
		uuid.v4 = jest.fn(() => "12");

		expect(parser.parseLine('Mollis consequat,9.00,2')).toStrictEqual({
            "id": "12",
            "name": "Mollis consequat",
            "price": 9,
            "quantity": 2
		});
		expect(uuid.v4).toHaveBeenCalledTimes(1);
	});

	// #2
	it('should return an array with an object with error in header, row and cols number, error message if header doesn`t match template', () => {
		const changedHeaderTitle = "Quan";

		expect(parser.validate(`Product name,Price,${changedHeaderTitle}`))
			.toStrictEqual(
				[
					parser.createError(parser.ErrorType.HEADER, 0, 2, `Expected header to be named "Quantity" but received ${changedHeaderTitle}.`)
				]
			);
	});

	// #3
	it('should return an array with an object with error in a row, row num, -1 instead of cell num, error message if cols count less than expected', () => {
		const cells = "Mollis consequat,9.00";
		const cellsLength = cells.split(/,/).length;
		expect(parser.validate(
		`Product name,Price,Quantity
		${cells}`
		))
			.toStrictEqual(
				[
					parser.createError(parser.ErrorType.ROW, 1, -1, `Expected row to have ${parser.schema.columns.length} cells but received ${cellsLength}.`)
				]
			);
	});

	// #4
	it('should return an array with an object with error in a cell, row num, cell num, error message if cell contain empty string instead of nonempty string', () => {
		const cell = "	";

		expect(parser.validate(
		`Product name,Price,Quantity
		${cell}, 9.00, 2`
		))
			.toStrictEqual(
				[
					parser.createError(parser.ErrorType.CELL, 1, 0, `Expected cell to be a nonempty string but received "${cell.trim()}".`)
				]
			);
	});

	// #5
	it('should return an array with an object with error in a cell, row num, cell num, error message if cell doesn`t contain positive number', () => {
		let wrongValueCells = [-5, "ab	"];
		wrongValueCells= wrongValueCells.map(el => typeof(el) === 'string' ? el.trim() : el);

		expect(parser.validate(
		  `Product name,Price,Quantity
		   Mollis consequat, 9.00, ${wrongValueCells[0]}`
		))
		  .toStrictEqual(
		    [
				parser.createError(parser.ErrorType.CELL, 1, 2, `Expected cell to be a positive number but received "${wrongValueCells[0]}".`)
			]
		  );
		
		expect(parser.validate(
		  `Product name,Price,Quantity
		   Mollis consequat, 9.00, ${wrongValueCells[1]}`
		))
		  .toStrictEqual(
			[
			  parser.createError(parser.ErrorType.CELL, 1, 2, `Expected cell to be a positive number but received "${wrongValueCells[1]}".`)
			]
		  );
	});

	// #6
	it('should return an empty array if validation is successfull', () => {

		expect(parser.validate(
		`Product name,Price,Quantity
		Mollis consequat, 9.00, 2`
		))
			.toStrictEqual([]);
	});

	// #7
	it('should throw an error if validation failed', () => {
		const fileContent = 
			`Product name,Price,Quantity
			Mollis consequat, 9.00`;
		parser.readFile = jest.fn(() => fileContent);

		expect(() => parser.parse())
			.toThrow('Validation failed!');

		expect(parser.readFile).toHaveBeenCalledTimes(1);		
	});

	// #8
	it('should return an object with items and total price if parsing is successfull', () => {
		const name = "Mollis consequat";
		const price = 9.00;
		const quantity = 2;
		const fileContent = 
			`Product name,Price,Quantity
			${name}, ${price}, ${quantity}`;
		parser.readFile = jest.fn(() => fileContent);
		parser.validate = jest.fn(() => []);

		expect(parser.parse())
			.toStrictEqual({
				items: [
					{
						id: "12",
						name,
						price,
						quantity
					}
				],
				total: price * quantity
			});

		expect(parser.readFile).toHaveBeenCalledTimes(1);
		expect(parser.validate).toHaveBeenCalledTimes(1);
	});

	// #9
	// Taking into account that readFile func may be developed in future, we must be confident that it returns exactly what we need
	// We don`t test the module func. We test that our func returns the value of the module function.
	it('should return parsed file, the same as parsed by readFileSync', () => {
		expect(parser.readFile("F:\\DudinCorp\\BSA2020-Testing\\samples\\cart.csv"))
		  .toStrictEqual(readFileSync("F:\\DudinCorp\\BSA2020-Testing\\samples\\cart.csv", 'utf-8', 'r'))
	});

	// #10
	// We are checking if parser constructor adds required fields to the class. 
	it('should have all properties that are extremely required for proper work of other functions', () => {
		expect(parser).toHaveProperty('ColumnType');
		expect(parser).toHaveProperty('ErrorType');
		expect(parser).toHaveProperty('schema');

	});

	/* *
	   * I have decided that createError and calcResult functions shouldn`t be tested, because they are too simple.
	*/
});

describe('CartParser - integration test', () => {
	// Add your integration test here.

	it('should return an object with items and total price if parsing is successfull, otherwise - throw an error', () => {
		expect(parser.parse('F:\\DudinCorp\\BSA2020-Testing\\samples\\cart.csv'))
			.toHaveProperty('items');
		expect(parser.parse('F:\\DudinCorp\\BSA2020-Testing\\samples\\cart.csv'))
			.toHaveProperty('total');


		const name = "Mollis consequat";
		const price = 9.00;
		const quantity = -2;
		const errorContent = 
			`Product name,Price,Quanity
			${name}, ${price}, ${quantity}`;
		parser.readFile = jest.fn(() => errorContent);

		expect(() => parser.parse())
			.toThrow();
	});
});